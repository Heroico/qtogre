#import <QApplication>
#import <QWidget>
#import <QVBoxLayout>
#import "qtogrewidget.h"

using namespace std;

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QWidget window;

    window.resize(800, 600);
    window.setWindowTitle("Simple example");

    QtOgreWidget* ogreWidget = new QtOgreWidget;

    QVBoxLayout *layout = new QVBoxLayout;
    layout->setContentsMargins(QMargins(0,0,0,0));
    layout->addWidget(ogreWidget);

    window.setLayout(layout);

    window.show();

    return app.exec();
}

