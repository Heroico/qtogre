#ifndef QTOGREWIDGET_H
#define QTOGREWIDGET_H

#include <OGRE/Ogre.h>
#ifdef OGRE_STATIC_LIB
#import <OGRE/RenderSystems/GL/OgreGLPrerequisites.h>
#import <OGRE/RenderSystems/GL/OgreGLPlugin.h>
#import <OGRE/OgreStaticPluginLoader.h>
#endif
#include <OGRE/Overlay/OgreOverlaySystem.h>
#include <QGLWidget>

class QtOgreWidget : public QGLWidget
{
    Q_OBJECT
public:
    QtOgreWidget( QWidget *parent=0 );

    virtual ~QtOgreWidget()
    {
        if (mOverlaySystem) delete mOverlaySystem;
        //mOgreRoot->shutdown();
        delete mOgreRoot;
        destroy();
    }

protected:
    virtual void initializeGL();
    virtual void resizeGL( int, int );
    virtual void paintGL();

    virtual void initializeResources();
    virtual void setUpScene();
    virtual void setUpRenderer();
    void init( std::string, std::string, std::string );

    virtual Ogre::RenderSystem* chooseRenderer( const Ogre::RenderSystemList & );

    Ogre::Root *mOgreRoot;
    Ogre::RenderWindow *mOgreWindow;
    Ogre::Camera *mCamera;
    Ogre::Viewport *mViewport;
    Ogre::SceneManager *mSceneMgr;
    Ogre::OverlaySystem *mOverlaySystem;
#ifdef OGRE_STATIC_LIB
    Ogre::GLPlugin *mGLPlugin;
#endif
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
   // Added for Mac compatibility
   Ogre::String                 m_ResourcePath;
#endif

signals:

public slots:
};

#endif // QTOGREWIDGET_H
