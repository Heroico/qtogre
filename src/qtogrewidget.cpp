#include "qtogrewidget.h"
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
#import <OGRE/OSX/macUtils.h>
#endif

QtOgreWidget::QtOgreWidget( QWidget *parent ):
    QGLWidget( parent ),
    mOgreWindow(NULL)
{
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE
    m_ResourcePath = Ogre::macBundlePath() + "/Contents/Resources/";
#elif defined(OGRE_IS_IOS)
    m_ResourcePath = Ogre::macBundlePath() + "/";
#else
    m_ResourcePath = "";
#endif
    Ogre::String ogreCFG = m_ResourcePath + "ogre.cfg";
    Ogre::String pluginsCFG = m_ResourcePath + "plugins.cfg";
    Ogre::String ogreLOG = "ogre.log";

    //Should we use a cfg?
    //init(pluginsCFG, ogreCFG, ogreLOG );
    init("", "", ogreLOG);
    //init("", ogreCFG, ogreLOG );
}

void QtOgreWidget::init( std::string plugins_file,
         std::string ogre_cfg_file,
         std::string ogre_log )
{
  // create the main ogre object
  mOgreRoot = new Ogre::Root( plugins_file, ogre_cfg_file, ogre_log );

 #ifdef OGRE_STATIC_LIB
  mGLPlugin = new Ogre::GLPlugin();
  mGLPlugin->install();
 #endif

  setUpRenderer();
  mOgreRoot->saveConfig();
  initializeResources();
  mOverlaySystem = new Ogre::OverlaySystem();
  mOgreRoot->initialise(false);
}

void QtOgreWidget::setUpRenderer() {
    // setup a renderer
    const Ogre::RenderSystemList &renderers = mOgreRoot->getAvailableRenderers();
    assert( !renderers->empty() ); // we need at least one renderer to do anything useful

    Ogre::RenderSystem *renderSystem;
    renderSystem = chooseRenderer( renderers );

    assert( renderSystem ); // user might pass back a null renderer, which would be bad!
    QString dimensions = QString( "%1x%2" )
                      .arg(this->width())
                      .arg(this->height());

    renderSystem->setConfigOption( "Video Mode", dimensions.toStdString() );
    // initialize without creating window
    renderSystem->setConfigOption( "Full Screen", "No" );

    mOgreRoot->setRenderSystem( renderSystem );
}

void QtOgreWidget::initializeGL()
{
  //== Creating and Acquiring Ogre Window ==//

  // Get the parameters of the window QT created
  Ogre::String winHandle;
#ifdef WIN32
  // Windows code
  winHandle += Ogre::StringConverter::toString((unsigned long)(this->parentWidget()->winId()));
#elif __APPLE__
  winHandle  += Ogre::StringConverter::toString((unsigned long)winId());
#else
  // Unix code
  QX11Info info = x11Info();
  winHandle  = Ogre::StringConverter::toString((unsigned long)(info.display()));
  winHandle += ":";
  winHandle += Ogre::StringConverter::toString((unsigned int)(info.screen()));
  winHandle += ":";
  winHandle += Ogre::StringConverter::toString((unsigned long)(this->parentWidget()->winId()));
#endif


  Ogre::NameValuePairList params;
#ifndef __APPLE__
  // code for Windows and Linux
  params["parentWindowHandle"] = winHandle;
  mOgreWindow = mOgreRoot->createRenderWindow( "QOgreWidget_RenderWindow",
                           this->width(),
                           this->height(),
                           false,
                           &params );

  mOgreWindow->setActive(true);
  WId ogreWinId = 0x0;
  mOgreWindow->getCustomAttribute( "WINDOW", &ogreWinId );

  assert( ogreWinId );

  // bug fix, extract geometry
  QRect geo = this->frameGeometry ( );

  // create new window
  this->create( ogreWinId );

  // set geometrie infos to new window
  this->setGeometry (geo);

#else
  // code for Mac
  params["externalWindowHandle"] = winHandle;
  params["macAPI"] = "cocoa";
  params["macAPICocoaUseNSView"] = "true";
  Ogre::LogManager::getSingleton().logMessage("handle"+winHandle);
  mOgreWindow = mOgreRoot->createRenderWindow("QOgreWidget_RenderWindow",
      width(), height(), false, &params);
  mOgreWindow->setActive(true);
  makeCurrent();
  makeOverlayCurrent();
#endif

  //remove autobuffer swap!
  setAutoBufferSwap(false);
  setAttribute( Qt::WA_PaintOnScreen, true );
  setAttribute( Qt::WA_NoBackground );

  //== Ogre Initialization ==//
  Ogre::SceneType scene_manager_type = Ogre::ST_EXTERIOR_CLOSE;

  mSceneMgr = mOgreRoot->createSceneManager( scene_manager_type );

  mSceneMgr->addRenderQueueListener(mOverlaySystem);
  mSceneMgr->setAmbientLight( Ogre::ColourValue(1,1,1) );

  mCamera = mSceneMgr->createCamera( "QOgreWidget_Cam" );
  mCamera->setPosition( Ogre::Vector3(0,0,100) );
  mCamera->lookAt( Ogre::Vector3(0,0,-100) );
  mCamera->setNearClipDistance( 1.0 );

  Ogre::Viewport *mViewport = mOgreWindow->addViewport( mCamera );
  mViewport->setBackgroundColour( Ogre::ColourValue( 0.8,0.6,1 ) );
  mCamera->setAspectRatio(Ogre::Real(mViewport->getActualWidth()) / Ogre::Real(mViewport->getActualHeight()));

  Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

  setUpScene();
  Ogre::LogManager::getSingleton().logMessage("** Initialize GL end");
}

void QtOgreWidget::initializeResources() {
    Ogre::String secName, typeName, archName;
    Ogre::ConfigFile cf;
    cf.load(m_ResourcePath + "resources.cfg");

    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;
#if OGRE_PLATFORM == OGRE_PLATFORM_APPLE || defined(OGRE_IS_IOS)
            // OS X does not set the working directory relative to the app,
            // In order to make things portable on OS X we need to provide
            // the loading with it's own bundle path location
            if (!Ogre::StringUtil::startsWith(archName, "/", false)) // only adjust relative dirs
                archName = Ogre::String(m_ResourcePath + archName);
#endif
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
        }
    }
}

void QtOgreWidget::setUpScene() {
#if 0
    Ogre::LogManager::getSingleton().logMessage("setupscene");
#else
    // Set the scene's ambient light
    mSceneMgr->setAmbientLight(Ogre::ColourValue(0.5f, 0.5f, 0.5f));

    // Create an Entity
    Ogre::Entity* ogreHead = mSceneMgr->createEntity("Head", "ogrehead.mesh");

    // Create a SceneNode and attach the Entity to it
    Ogre::SceneNode* headNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("HeadNode");
    headNode->attachObject(ogreHead);

    // Create a Light and set its position
    Ogre::Light* light = mSceneMgr->createLight("MainLight");
    light->setPosition(20.0f, 80.0f, 50.0f);
#endif
}

void QtOgreWidget::paintGL()
{
  Ogre::LogManager::getSingleton().logMessage("** Paint GL");
  if (!mOgreRoot->isInitialised())
      return;
  swapBuffers();
  assert( mOgreWindow );
  mOgreRoot->renderOneFrame();
}

void QtOgreWidget::resizeGL( int width, int height )
{
   //assert( mOgreWindow );
   //mOgreWindow->windowMovedOrResized();
   assert( mOgreWindow );
   mOgreWindow->reposition( this->pos().x(),
                            this->pos().y() );
   mOgreWindow->resize( width, height );
   updateGL();
}

Ogre::RenderSystem* QtOgreWidget::chooseRenderer( const Ogre::RenderSystemList &renderers )
{
  // It would probably be wise to do something more friendly
  // that just use the first available renderer
  return *renderers.begin();
}
